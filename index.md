Chitayl Notes
===================


1. Installation of WSL 
-  WSL is windows system for Linux

2. Dual boot your computer
- Ubuntu
- Windows


3. Editor

Emacs, Vim, VSCODE

4. Markdown


5. Python  
   
   In python, implement data structures in the language

 
6. Enroll into Coursera courses
   -
     https://www.coursera.org/learn/python-data/supplement/0gpo0/course-syllabus
	 
7. GIT
   - create an account on gitlab.com
   - Documentation - https://git-scm.com/


8. Emails
   - poojithaeduru93@gmail.com
   - chandanaaavula@gmail.com
